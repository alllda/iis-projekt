<?
    $open = 0;
    if( $_GET["option"]=='pridej-hw' || $_GET["option"]=='odebrat-hw' || $_GET["option"]=='vadny-hw' || $_GET['option'] == 'upravit-hw' || $_GET['option'] == 'opravit-hw' )
        $open = 1;

	if( $_GET["option"]=='pridat-kancelar' || $_GET["option"]=='odebrat-kancelar' || $_GET["option"]=='upravit-kancelar' || $_GET["option"]=='kancelar-uzivatel' || $_GET["option"]=='kancelar-prehled')
        $open = 2;

	if( $_GET["option"]=='pridat-ucebna' || $_GET["option"]=='odebrat-ucebna' || $_GET["option"]=='upravit-ucebna' )
        $open = 3;

	if( $_GET["option"]=='upravit-technika' || $_GET["option"]=='odebrat-technika' || $_GET["option"]=='upravit-uzivatele' || $_GET["option"]=='odebrat-uzivatele' || $_GET["option"]=='pridej-technik')
		$open = 4;
?>

<div class="col-sm-4 col-md-2 col-xs-12 sidebar">
<ul class="nav nav-sidebar nav-pills nav-stacked nav-default">
    <li <? if( !isset($_GET['option']) && ($_GET['str'] == 'technik' || $_GET['str'] == 'home')) echo 'class="active"'; ?>><a href="index.php?str=technik">Overview</a></li>
    <li class=" dropdown">
        <a data-toggle="collapse" data-parent="#menu-bar" href="#collapseHW">Hardware<span class="caret"></span></a>
        <ul id="collapseHW" class="panel-collapse collapse <?if($open == 1) echo 'in'; ?> nav nav-sidebar nav-pills nav-stacked">
            <li class="indentMenu <?if($_GET["option"]=='pridej-hw')echo"active"; ?>"><a href="index.php?str=technik&option=pridej-hw">Přidat</a></li>
            <li class="indentMenu <?if($_GET["option"]=='odebrat-hw')echo"active"; ?>"><a href="index.php?str=technik&option=odebrat-hw">Odebrat</a></li>
            <li class="indentMenu <?if($_GET["option"]=='upravit-hw')echo"active"; ?>"><a href="index.php?str=technik&option=upravit-hw">Upravit</a></li>
            <li class="indentMenu <?if($_GET["option"]=='opravit-hw')echo"active"; ?>"><a href="index.php?str=technik&option=opravit-hw">Opravit</a></li>
            <li class="indentMenu <?if($_GET["option"]=='vadny-hw')echo"active"; ?>"><a href="index.php?str=technik&option=vadny-hw">Vadný</a></li>
        </ul>
    </li>

    <li class=" dropdown">
        <a data-toggle="collapse" data-parent="#menu-bar" href="#collapseKancl">Kancelář<span class="caret"></span></a>
        <ul id="collapseKancl" class="panel-collapse collapse <?if($open == 2) echo 'in'; ?> nav nav-sidebar nav-pills nav-stacked">
            <li class="indentMenu <?if($_GET["option"]=='pridat-kancelar')echo"active"; ?>"><a href="index.php?str=technik&option=pridat-kancelar">Přidat</a></li>
            <li class="indentMenu <?if($_GET["option"]=='kancelar-uzivatel')echo"active"; ?>"><a href="index.php?str=technik&option=kancelar-uzivatel">Přidat uživatele</a></li>
            <li class="indentMenu <?if($_GET["option"]=='kancelar-prehled')echo"active"; ?>"><a href="index.php?str=technik&option=kancelar-prehled">Přehled kanceláří</a></li>
            <li class="indentMenu <?if($_GET["option"]=='odebrat-kancelar')echo"active"; ?>"><a href="index.php?str=technik&option=odebrat-kancelar">Odebrat</a></li>
            <li class="indentMenu <?if($_GET["option"]=='upravit-kancelar')echo"active"; ?>"><a href="index.php?str=technik&option=upravit-kancelar">Upravit</a></li>
        </ul>
    </li>

    <li class=" dropdown">
        <a data-toggle="collapse" data-parent="#menu-bar" href="#collapseUcebna">Učebna<span class="caret"></span></a>
        <ul id="collapseUcebna" class="panel-collapse collapse <?if($open == 3) echo 'in'; ?> nav nav-sidebar nav-pills nav-stacked">
            <li class="indentMenu <?if($_GET["option"]=='pridat-ucebna')echo"active"; ?>"><a href="index.php?str=technik&option=pridat-ucebna">Přidat</a></li>
            <li class="indentMenu <?if($_GET["option"]=='odebrat-ucebna')echo"active"; ?>"><a href="index.php?str=technik&option=odebrat-ucebna">Odebrat</a></li>
            <li class="indentMenu <?if($_GET["option"]=='upravit-ucebna')echo"active"; ?>"><a href="index.php?str=technik&option=upravit-ucebna">Upravit</a></li>
        </ul>
    </li>
  <li class=" dropdown">
        <a data-toggle="collapse" data-parent="#menu-bar" href="#collapseSpravaUzivatelu">Správa uživatelů<span class="caret"></span></a>
        <ul id="collapseSpravaUzivatelu" class="panel-collapse collapse <?if($open == 4) echo 'in'; ?> nav nav-sidebar nav-pills nav-stacked">
			<li class="indentMenu <?if($_GET["option"]=='pridej-technik')echo"active"; ?>"><a href="index.php?str=technik&option=pridej-technik">Registrovat technika</a></li>
            <li class="indentMenu <?if($_GET["option"]=='upravit-technika')echo"active"; ?>"><a href="index.php?str=technik&option=upravit-technika">Upravit technika</a></li>
            <li class="indentMenu <?if($_GET["option"]=='odebrat-technika')echo"active"; ?>"><a href="index.php?str=technik&option=odebrat-technika">Odebrat technika</a></li>
            <li class="indentMenu <?if($_GET["option"]=='upravit-uzivatele')echo"active"; ?>"><a href="index.php?str=technik&option=upravit-uzivatele">Upravit uživatele</a></li>
			<li class="indentMenu <?if($_GET["option"]=='odebrat-uzivatele')echo"active"; ?>"><a href="index.php?str=technik&option=odebrat-uzivatele">Odebrat uživatele</a></li>
        </ul>
    </li>

</ul>
</div>  
