<?php 
if(isset($_POST['odeslat'])){
	if($_POST['options'] == '1'){
	echo 'prvni moznost';
		mysql_query('update uzivatel set jmeno = "' . $_POST['jmeno'] . '", prijmeni = "' . $_POST['prijmeni'] . '" where uziv_id = ' . $_POST['odeslat'] . ' ', $link);
	}
	else if(!empty($_POST['heslo'])){	
echo 'druha moznost';
		mysql_query('update technik set jmeno = "' . $_POST['jmeno'] . '", prijmeni = "' . $_POST['prijmeni'] . '", heslo = "' . $_POST['heslo'] . '" where uziv_id = ' . $_POST['odeslat'] . ' ', $link);
	}
}
?>


<h2>Upravit uzivatele</h2>
<br />
<table class="table table-hover">
	<thead>
		<tr>
			<th>Jméno</th>
			<th>Příjmení</th>
			<th>Login</th>
			<th>Kancelář</th>
			<th>Upravit</th>
		</tr>
	</thead>
	<tbody>

<?php
$result = mysql_query("select * from uzivatel natural left join (select * from kancelar, uzivatel where kancelar_id = k_id) a", $link);

if(mysql_num_rows($result) > 0){
	while($row = mysql_fetch_array($result)){
		echo '<tr class="success"><td>' . $row['jmeno'] . '</td><td>' . $row['prijmeni'] . '</td><td>' . $row['login_u'] . '</td><td>';
		if($row['blok_k'] != NULL)
			echo $row['blok_k'] . $row['patro_k'] . '.' . $row['k_id'];
		else
			echo '-';
		echo '</td><td><button class="btn btn-default btn-sm" data-toggle="modal" data-target="#myModal"  name="uprav" value="' . $row['uziv_id'] . '">Uprav</button></td></tr>';
	}
}
else{
	echo '<tr class="danger"><td colspan="4">Žádný uživatel nebyl nalezen</td></tr>';
}

?>
</tbody>
</table>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Upravit technika</h4>
			</div>
			<div class="modal-body">
			<form class="form-horizontal" role="form" method="post">
	            <div class="form-group">
					<label class="col-sm-2 control-label">Jméno</label>
					<div class="col-sm-10">
                       	<input type="text" name="jmeno" id="jmeno" class="form-control input-lg" tabindex="1" required>
					</div>
               	</div> 
			    <div class="form-group">
					<label class="col-sm-2 control-label">Příjmení</label>
					<div class="col-sm-10">
                       	<input type="text" name="prijmeni" id="prijmeni" class="form-control input-lg" tabindex="2" required>
					</div>
            	</div>
				<div style="margin-left: 17%;">
		 			<div class="row" style="margin: 0 0 1.5% 0.5%;">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-primary active btn-lg">
								<input type="radio" name="options" id="nemenit" value="1" checked>Neměnit heslo
							</label>
							<label class="btn btn-primary btn-lg">
								<input type="radio" name="options" id="zmenit" value="2">Změnit heslo
							</label>
						</div>
					</div>
				</div>
                <div class="form-group hesla" style="display:none">
					<label class="col-sm-2 control-label">Heslo</label>
					<div class="col-sm-10">
                       	<input type="text" name="heslo" id="heslo" class="form-control input-lg" tabindex="3">
					</div>
            	</div> 
                   	
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" name="odeslat">Odeslat</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			</form>
		</div>
	</div>
</div>


<script>
$(document).ready(function () {
	$('tr button[name=uprav]').click(function() {
		$('#jmeno').val($(this).parent().parent().children().eq(0).html());	
		$('#prijmeni').val($(this).parent().parent().children().eq(1).html());
		$('button[name=odeslat]').val($(this).attr('value'));

		$("#nemenit").change(function () {
			$(".hesla").attr("style","display: none;");
		});

		$("#zmenit").change(function () {
			$(".hesla").removeAttr("style");
		});

	});
 });
</script>
