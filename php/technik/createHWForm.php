

<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
        <form role="form" method="post">
            <h2>Nový Hardware</h2>
            <hr class="colorgraph">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9">
                    <div class="form-group">
                        <input type="text" name="name" id="name inputWarning1" 
                         class="form-control input-lg" placeholder="Název" tabindex="1" required>
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9">
                    <div class="form-group">
                        <input type="text" name="type" id="type inputWarning1" 
                         class="form-control input-lg" placeholder="Typ" tabindex="2" required>
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9">
                    <div class="form-group">
                        <input type="text" name="vyrobce" id="vyrobce inputWarning1" 
                         class="form-control input-lg" placeholder="Výrobce" tabindex="3" required>
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9">
                    <div class="form-group">
                        <input type="number" min="0" name="price" id="price inputWarning1" 
                         class="form-control input-lg" placeholder="Cena (zaokrouhlete na Kč)" tabindex="4" required>
                    </div>
                </div>
                
            </div>
			<div class="row" style="margin: 0 0 1.5% 0.5%;">
				<div class="btn-group" data-toggle="buttons">
					<label class="btn btn-primary active btn-lg">
						<input type="radio" name="options" id="uzivatel_radio1" value="1" checked> Uživatel
					</label>
					<label class="btn btn-primary btn-lg">
						<input type="radio" name="options" id="uzivatel_radio2" value="2"> Učebna
					</label>
					<label class="btn btn-primary btn-lg">
						<input type="radio" name="options" id="uzivatel_radio3" value="3"> Volný
					</label>
				</div>
			</div>
			<div class="row">
	            <div class="form-group uzivatel_select">
    	             <div class="col-xs-12 col-sm-12 col-md-9">
        	       		<select class="form-control input-lg" name="uzivatel" id="uzivatel">
            	                 <?
                	                 $ret = mysql_query("select * from uzivatel");
                    	             if(mysql_num_rows($ret) > 0){
                        	             while($row = mysql_fetch_array($ret)){
                            	             echo "<option value='". $row['uziv_id'] ."'>". $row['jmeno'] ." ". $row['prijmeni'] ."</option>";
                                	     }
	                                 }
    	                         ?>
        	             </select>
            	     </div>
	             </div>                       
    	         <div class="form-group ucebna_select" style="display: none;">
        	        <div class="col-xs-12 col-sm-12 col-md-9">
            	  		<select class="form-control input-lg" name="ucebna" id="ucebna">
                	            <?
                    	            $ret = mysql_query("select * from ucebnaCVT");
                        	        if(mysql_num_rows($ret) > 0){
                            	        while($row = mysql_fetch_array($ret)){
                                	        echo "<option value='". $row['ucebna_id'] ."'>". $row['blok'] ."". $row['patro'] .".". $row['ucebna_id'] . "</option>";
                                    	}
	                                }
    	                        ?>
        	            </select>
            	    </div>
	            </div>
			</div>
            <hr class="colorgraph">
            <div class="row">
                <div class="col-xs-12 col-md-9"><input type="submit" value="Vytvořit HW" class="btn btn-primary btn-block btn-lg" tabindex="6"></div>
            </div>
        </form>
    

<?php
	if(isset($_POST['name'])){
		mysql_query("insert into hardware (nazev,typ,vyrobce,cena) values('".$_POST["name"]."','".$_POST["type"]."','".$_POST["vyrobce"]."','".$_POST["price"]."')",$link);
		$id = mysql_insert_id($link);
		if($_POST["options"] == "1"){
			mysql_query("insert into vlastnik (id_HW,id_uzivatele) values ('".$id."','".$_POST["uzivatel"]."')");
		}
		else if($_POST['options'] == "2"){
			mysql_query("insert into vlastnik (id_HW,id_ucebny) values ('".$id."','".$_POST["ucebna"]."')");
		}
	
	
    echo '<p class="bg-warning" style="font-size: 20px; padding: 20px 20px 20px 20px; margin-right: 50%">
			<button type="button" class="close" data-dismiss="alert">
 				 <span aria-hidden="true">&times;</span>
 				 <span class="sr-only">Close</span>
			</button>
			Hardware byl uspěšně přidán
		</p>';

	}
?>
	</div>



<script>
$(document).ready(function () {
	$("#uzivatel_radio1").change(function () {
		$(".ucebna_select").attr("style","display: none;");
		$(".uzivatel_select").removeAttr("style");
	});
	$("#uzivatel_radio2").change(function () {
		$(".ucebna_select").removeAttr("style");
		$(".uzivatel_select").attr("style","display: none;");
	});
	$("#uzivatel_radio3").change(function () {
		$(".ucebna_select").attr("style","display: none;");
		$(".uzivatel_select").attr("style","display: none;");
	});
});
</script>
