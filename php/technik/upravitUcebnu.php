<?php 
if(isset($_POST['odeslat'])){
	$result = mysql_query("select * from ucebnaCVT where ucebna_id = ". $_POST['odeslat'], $link);
	if( mysql_num_rows($result) == 1 ){
		$row = mysql_fetch_array($result);
		mysql_query('update ucebnaCVT set spravce_id = "' . $_POST['technik'] . '", blok = "' . $_POST['blok'] . '", patro = "' . $_POST['patro'] . '", kapacita = "' . $_POST['kapacita'] . '", pocet_tabuli = "' . $_POST['tabule'] . '"  where ucebna_id = ' . $_POST['odeslat'] . ' ', $link);
	}
}
?>


<h2>Upravit učebnu</h2>
<br />
<table class="table table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>Blok</th>
			<th>Patro</th>
			<th>Kapacita</th>
			<th>Počet tabulí</th>
			<th>Správce</th>
			<th>Upravit</th>
		</tr>
	</thead>
	<tbody>

<?php
$result = mysql_query("select * from ucebnaCVT, technik where ucebnaCVT.spravce_id = technik.tech_id order by ucebnaCVT.ucebna_id", $link);

if(mysql_num_rows($result) > 0){
	while($row = mysql_fetch_array($result)){
		echo '<tr class="success"><td>' . $row['ucebna_id'] . '</td><td>' . $row['blok'] . '</td><td>' . $row['patro'] . '</td><td>' . $row['kapacita'] . '</td><td>' . $row['pocet_tabuli'] . '</td><td>' . $row['jmeno_t'] . ' ' . $row['prijmeni_t'] . '</td><td><button class="btn btn-default btn-sm" data-toggle="modal" data-target="#myModal"  name="uprav" value="' . $row['ucebna_id'] . '">Uprav</button></td></tr>';
	}
}
else{
	echo '<tr class="danger"><td colspan="4">Nevlastníte žádné zařízení</td></tr>';
}

?>
</tbody>
</table>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Upravit hardware</h4>
			</div>
			<div class="modal-body">
			<form class="form-horizontal" role="form" method="post">
	            <div class="form-group">
                    <label for="patro" class="col-sm-2 control-label">Patro</label>
					<div class="col-sm-10">
                    	<select class="form-control" name="patro" id="patro">
                   			<option>1</option>
	                        <option>2</option>
    	                    <option>3</option>
        	                <option>4</option>
            	            <option>5</option>
                	        <option>6</option>
                    	</select>
					</div>
                </div>
               
                <div class="form-group">
                    <label for="blok" class="col-sm-2 control-label">Blok</label>
                    <div class="col-sm-10">
                    	<select class="form-control" name="blok" id="blok">
                        	<option>A</option>
                            <option>B</option>
                            <option>C</option>
                            <option>D</option>
                            <option>E</option>
                            <option>F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="technik" class="col-sm-2 control-label">Technik</label>
                    <div class="col-sm-10">
                  		<select class="form-control" name="technik" id="technik">
                                <?
                                    $ret = mysql_query("select * from technik");
                                    echo "Pocet: " . mysql_num_rows($ret);
                                    if(mysql_num_rows($ret) > 0){
                                        while($row = mysql_fetch_array($ret)){
                                            echo "<option value='". $row['tech_id'] ."'>". $row['jmeno_t'] ." ". $row['prijmeni_t'] ."</option>";
                                        }
                                    }


                                ?>
                        </select>
                    </div>
                </div>
		     	<div class="form-group">
					<label class="col-sm-2 control-label">Počet tabulí</label>
					<div class="col-sm-10">
                       	<input type="number" min="0" name="tabule" id="tabule" class="form-control input-lg" tabindex="1" required>
					</div>
               	</div> 
			    <div class="form-group">
					<label class="col-sm-2 control-label">Kapacita</label>
					<div class="col-sm-10">
                       	<input type="number" min="0" name="kapacita" id="kapacita" class="form-control input-lg" tabindex="2" required>
					</div>
            	</div> 
                   
                   	
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" name="odeslat">Odeslat</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			</form>
		</div>
	</div>
</div>


<script>
$(document).ready(function () {
	$('tr button[name=uprav]').click(function() {
		$('#kapacita').val($(this).parent().parent().children().eq(3).html());	
		$('#patro').children().eq($(this).parent().parent().children().eq(2).html()-1).attr('selected','selected');
		$('#tabule').val($(this).parent().parent().children().eq(4).html());
		for( var i = 0; i < 6; i++){
			if($('#blok').children().eq(i).html() == $(this).parent().parent().children().eq(1).html()){
				$('#blok').children().eq(i).attr('selected','selected');
			}
		}
		for( var i = 0; i < $('#technik option').size(); i++){
			if($('#technik').children().eq(i).html() == $(this).parent().parent().children().eq(5).html()){
				$('#technik').children().eq(i).attr('selected','selected');
			}
		}
		$('button[name=odeslat]').val($(this).attr('value'));
	});
 });
</script>
