<h2>Výpis vadného hardware</h2>
<br />
<table class="table table-hover">
	<thead>
		<tr>
			<th>Nahlášeno</th>
			<th>Datum nahlášení</th>
			<th>Datum vyřízení</th>
			<th>Kancelář</th>
			<th>Učebna</th>
			<th>Zařízení</th>
			<th>Výrobce</th>
			<th>Typ</th>
			<th>Popis problému</th>
		</tr>
	</thead>
	<tbody>

<?php
$result = mysql_query("SELECT distinct vlastnik.id_ucebny, hardware.*, oprava.datum_vlozeni, oprava.datum_vyrizeni, oprava.poznamka, uzivatel.* FROM hardware, oprava, uzivatel, vlastnik WHERE oprava.id_hardware = hardware.hw_id and uzivatel.uziv_id = oprava.id_uzivatele and vlastnik.id_HW = oprava.id_hardware", $link);

if(mysql_num_rows($result) > 0){
	while($row = mysql_fetch_array($result)){
		echo '<tr class="';
		if($row['datum_vyrizeni'] == 0)
			echo 'danger';
		else
			echo 'success';
		echo '"><td>' . $row['jmeno'] . ' ' . $row['prijmeni'] . '</td><td>' . $row['datum_vlozeni'] . '</td><td>';
		if($row['datum_vyrizeni'] == 0)
			echo ' - ';
		else
			echo $row['datum_vyrizeni'];

		echo '</td><td>';
		if($row['id_ucebny'] == null){
			echo $row['kancelar_id'];
		}
		else{
			echo '-';
		}
		echo '</td><td>';
		if($row['id_ucebny'] == null)
			echo ' - ';
		else
			echo $row['id_ucebny'];

		echo '</td><td>' . $row['nazev'] . '</td><td>' . $row['vyrobce'] . '</td><td>' . $row['typ'] . '</td><td>' . $row['poznamka'] . '</td></tr>';
	}
}
else{
	echo '<tr class="danger"><td colspan="4">Neexistuje žádné vadné zařízení</td></tr>';
}

?>

	</tbody>
</table>
