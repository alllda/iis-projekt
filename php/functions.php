<?

define("TECHNIK", 1);
define("UZIVATEL",2);

function checkPermission($role){
	if(empty($_SESSION["role"])){
		session_destroy();
		echo '<script type="text/javascript">window.location = "index.php?noPermission=true"</script>';
	}
	else if($_SESSION["role"] == $role)
		return;
	else{
		echo '<script type="text/javascript">window.location = "/index.php?str=php/noPermission"</script>';
	}
}

function correctInputData(){
	if(!empty($_POST)){
		foreach($_POST as $key => &$value) {
			//echo "POST parameter '$key' has '$value';
			$value = htmlspecialchars($value);
		}
	}
	if(!empty($_GET)){
		foreach($_POST as $key => &$value){
			$value = htmlspecialchars($value);
		}
	}
}

function isAlpha($str){
	return preg_match("/^[\x80-\xFFa-zA-Z]+$/",preg_replace('/(^\s+)|($\s+)/', '', $str));
}

?>
