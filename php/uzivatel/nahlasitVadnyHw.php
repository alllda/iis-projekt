<h1>Nahlásit vadný hardware</h1>


<?php 
if(isset($_POST['odeslat'])){
	$result = mysql_query("select * from oprava where id_hardware = ". $_POST['odeslat'] . " and datum_vyrizeni = 0");
	if( mysql_num_rows($result) == 0 ){
		mysql_query("insert into oprava(`id_hardware`, `id_uzivatele`, `poznamka`) values(" . $_POST['odeslat'] . "," . $_SESSION["id"] . ',"' . $_POST['coment'] .'")', $link);
	}
}


if(isset($_POST['odhlas'])){
	mysql_query("delete from oprava where id_opravy = " . $_POST['odhlas'], $link);
}
?>
<br />
<h2>Moje zařízení</h2>
<br />
<table class="table table-hover">
	<thead>
		<tr>
			<th>Zařízení</th>
			<th>Výrobce</th>
			<th>Typ</th>
			<th>Nahlášení</th>
		</tr>
	</thead>
	<tbody>

<?php
$result = mysql_query("SELECT distinct hardware.* FROM uzivatel, hardware, vlastnik, oprava WHERE uzivatel.uziv_id = '" . $_SESSION["id"] . "' and vlastnik.id_uzivatele = uzivatel.uziv_id and hardware.hw_id = id_HW and hardware.hw_id not in ( select id_hardware from oprava where datum_vyrizeni = '0')", $link);

if(mysql_num_rows($result) > 0){
	while($row = mysql_fetch_array($result)){
		echo '<tr class="success"><td>' . $row['nazev'] . '</td><td>' . $row['vyrobce'] . '</td><td>' . $row['typ'] . '</td><td><button class="btn btn-default btn-sm"  name="nahlas" value="' . $row['hw_id'] . '" data-toggle="modal" data-target="#myModal">Nahlas</button></td></tr>';
	}
}
else{
	echo '<tr class="danger"><td colspan="4">Nevlastníte žádné zařízení</td></tr>';
}

?>

	</tbody>
</table>
<br />
<br />
<br />
<h2>Zařízení v učebnách</h2>
<br />
<table class="table table-hover">
	<thead>
		<tr>
			<th>Číslo učebny</th>
			<th>Blok</th>
			<th>Zařízení</th>
			<th>Výrobce</th>
			<th>Typ</th>
			<th>Nahlášení</th>
		</tr>
	</thead>
	<tbody>

<?php
$result = mysql_query("SELECT distinct hardware.*, ucebnaCVT.*  FROM hardware, vlastnik, ucebnaCVT, oprava WHERE  ucebnaCVT.ucebna_id = vlastnik.id_ucebny and hardware.hw_id = vlastnik.id_HW and hardware.hw_id not in ( select id_hardware from oprava where datum_vyrizeni = '0') and oprava.id_uzivatele = '" . $_SESSION["id"] . "'", $link);
if(mysql_num_rows($result) > 0){
	while($row = mysql_fetch_array($result)){
		echo '<tr class="success"><td>' . $row['ucebna_id'] . '</td><td>' . $row['blok'] . '</td><td>' . $row['nazev'] . '</td><td>' . $row['vyrobce'] . '</td><td>' . $row['typ'] . '</td><td><button name="nahlas" class="btn btn-default btn-sm" value="' . $row['hw_id'] . '" data-toggle="modal" data-target="#myModal">Nahlas</button></tr>';
	}
}
else{
	echo '<tr class="danger"><td colspan="6">V žádné učebně nebyl nalezen žádný hardware</td></tr>';
}
?>

	</tbody>
</table>

<br />
<br />
<br />

<h2>Mnou nahlášené zařízení k opravě</h2>
<br />
<form method="post">
<table class="table table-hover">
	<thead>
		<tr>
			<th>Datum vložení</th>
			<th>Datum vyřízení</th>
			<th>Zařízení</th>
			<th>Výrobce</th>
			<th>Typ</th>
			<th>Odhlášení</th>
		</tr>
	</thead>
	<tbody>

<?php

$result = mysql_query("SELECT DISTINCT * FROM hardware, oprava WHERE hardware.hw_id = oprava.id_hardware and oprava.datum_vyrizeni = 0 and oprava.id_uzivatele = '".$_SESSION["id"]."' ORDER BY datum_vlozeni DESC;", $link);
if(mysql_num_rows($result) > 0){
	while($row = mysql_fetch_array($result)){
		echo '<tr class="success"><td>' . $row['datum_vlozeni'] . '</td><td>';
		if($row['datum_vyrizeni'] == 0){
			echo ' - ';
		}
		else{
			echo $row['datum_vyrizeni'];
		}
		
		echo '</td><td>' . $row['nazev'] . '</td><td>' . $row['vyrobce'] . '</td><td>' . $row['typ'] . '</td><td><button type="submit" class="btn btn-default btn-sm" name="odhlas" value="' . $row['id_opravy'] . '">Odhlas</button></tr>';
	}
}
else{
	echo '<tr class="danger"><td colspan="6">Nebyl nalezen žádný Vámi hlášený hardware k opravě</td></tr>';
}
?>

	</tbody>
</table>
</form>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Upravit hardware</h4>
				</div>
				<div class="modal-body">
				<form class="form-horizontal" role="form" method="post">
            		<div class="form-group">
						<label class="col-sm-2 control-label">Poznámka</label>
						<div class="col-sm-10">
    	                   	<input type="text" name="coment" id="coment" class="form-control input-lg" tabindex="1" required>
						</div>
            	    </div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" name="odeslat">Odeslat</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
$(document).ready(function () {
	$('tr button[name=nahlas]').click(function() {
		$('button[name=odeslat]').val($(this).attr('value'));
	});
 });
</script>

