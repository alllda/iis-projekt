<?
if(!empty($_GET['login']))
    $login = "value='" . $_GET['login']."'";
else
    $login = "";
?>

<div class="container">

<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
        <form role="form" method="post" action="php/uzivatel/checkUserReg.php">
            <h2>Prosím registruj se</h2>
            <hr class="colorgraph">
            <div class="row">
                <div class="col-xs-12 col-sm-2 col-md-2">
                    <div class="form-group">
                        <input type="text" name="degree" id="degree" <? if(!empty($_GET["degree"]))echo 'value='.$_GET["degree"].""; ?>
                         class="form-control input-lg" placeholder="Titul" tabindex="1" >
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5">
                    <div class="form-group">
                        <input type="text" name="first_name" id="first_name" <? if(!empty($_GET["name"]))echo 'value='.$_GET["name"].""; ?>
                         class="form-control input-lg" placeholder="Jméno" tabindex="2" required>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5">
                    <div class="form-group">
                        <input type="text" name="last_name" id="last_name" <? if(!empty($_GET["lastName"]))echo 'value='.$_GET["lastName"].""; ?>
                         class="form-control input-lg" placeholder="Příjmení" tabindex="3" required>
                    </div>
                </div>
            </div>
            <div class="form-group <?if($_GET["existinUser"] == true)echo'has-error';?>">
                <?if($_GET["existinUser"] == true){
                    echo '<input type="text" id="inputWarning" name="login" id="display_name" class="form-control input-lg" placeholder="Login" tabindex="4" required>';
                    echo'<span class="help-block">Uživatel s takovým loginem již existuje</span>';
                }
                else
                    echo '<input type="text" name="login" id="display_name" '.$login.'class="form-control input-lg" placeholder="Login" tabindex="4" required>';  
                ?>
            </div>
           <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group <?if($_GET["badPassword"] == true)echo'has-error';?>">
                        <?if($_GET["badPassword"] == true){
                            echo '<input type="password" id="inputWarning" name="password" id="password" class="form-control input-lg" placeholder="Heslo" tabindex="5" required>';
                            echo'<span class="help-block">Heslo nesplnuje pozadavky (min 8 znaků)</span>';
                        }
                        else
                            echo '<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Heslo" tabindex="5" required>';  
                        ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group <?if($_GET["notSamePassword"] == true)echo'has-error';?>">
                        
                        <?if($_GET["notSamePassword"] == true){
                            echo '<input type="password" id="inputWarning" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Potvrzení hesla" tabindex="6">';
                            echo'<span class="help-block">Hesla nejsou stejná</span>';
                        }
                        else
                            echo '<input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Potvrzení hesla" tabindex="6">';  
                        ?>
                    </div>
                </div>
            </div>
            
            
            <hr class="colorgraph">
            <div class="row">
                <div class="col-xs-12 col-md-12"><input type="submit" value="Registrovat" class="btn btn-primary btn-block btn-lg" tabindex="6"></div>
            </div>
        </form>
    </div>
</div>
