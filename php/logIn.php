<div class="container">

      <form class="form-signin" role="form" method="post" action="php/logInCheck.php">
        <h2 class="form-signin-heading">Prosím přihlaste se</h2>
        <input type="login" class="form-control" placeholder="Login" required autofocus name="login">
        <input type="password" class="form-control" placeholder="Password" required name="pass">
        <label class="checkbox">
          <input type="checkbox" name="remember" value="remember-me"> Pamatuj si mě
        </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
      

    </div> <!-- /container -->
<div class="logInMessage">
          <?
          if($_GET["err"] == "userNotFound")
            echo "<h3>User not found</h3>";
          if($autoLogOut)
            echo "<h3>Vypršel časový limit</h3><br><h4>Přihlaste se znova</h4>";
          if($_GET["succesfullLogIn"] == "true")
            echo "<h3>Registrace proběhla úspěšně</h3>";          
          ?>
</div>
