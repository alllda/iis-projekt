<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="index.php?str=home">Home</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
      <? 
      if(isset($_SESSION["name"])){
        echo "<span class='navbar-text'>Ahoj ".$_SESSION["name"]."</span>";

      }
      ?>
        <li class="divider-vertical"></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Nabídka <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?
            if(!isset($_SESSION["role"])){
                echo '<li><a href="index.php?str=registerUser">Registrovat se</a></li>';
                echo '<li><a href="index.php?str=logIn">Přihlásit se</a></li>';
            }
            if(isset($_SESSION["role"])){
                echo '<li><a href="php/logOut.php">Odhlásit se</a></li>';
            }
            ?>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
