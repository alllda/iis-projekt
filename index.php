<?php
	setlocale(LC_CTYPE, 'cs_CZ.UTF-8');
    session_start();
    $autoLogOut = False;
    if(isset($_SESSION["time"])){
    	if(time() - $_SESSION["time"] > 1800){
    		$autoLogOut = True;
    		session_destroy();
    	}
    	else{
    		$_SESSION["time"] = time();
    	}
    }
	include('php/functions.php');
	correctInputData();
	include('php/connect.php');	//pripojeni k db, trva do konce provadeni skriptu
	echo "<html>\n";
		include('php/head.php');	//obsahuje celou hlavicku html
	echo "<body>\n";
	include('php/navigation.php');	//navigacni top panel
	echo "<div class='container-fluid'>\n";
		
	echo "<div id='body'>";
if($autoLogOut){
	include "php/logIn.php";
}
else if(!empty($_GET)){
    $str=$_GET['str'];
    if(!isset($str) && !isset($_SESSION["role"])){
        include "php/logIn.php";
    }
    else{
        if ($str == "logIn" && !isset($_SESSION["role"])){
            include "php/logIn.php";
        }
        else if($str == "registerUser" && !isset($_SESSION["role"])){
            include "php/uzivatel/registerUser.php";
        }
        else if($str == "home" && !isset($_SESSION["role"]))
            include "php/logIn.php";
        else{
            
			echo '<div class="row">';
            
            if($_SESSION["role"] == 1){ // technik
                if($str == "technik" || $str == "logIn" || $str == "home"){
                    $str = "php/technik/technik";
					if(isset($_GET['option']) && !empty($_GET['option'])){		//pouze sablona, zatim nevyplnovano
						switch($_GET['option']){
							case 'pridej-technik':
								$str = 'php/technik/createTechnicForm';
							break;
							
							case 'upravit-technika':
								$str = 'php/technik/upravitTechnika';
							break;

							case 'odebrat-technika':
								$str = 'php/technik/odebratTechnika';
							break;

							case 'upravit-uzivatele':
								$str = 'php/technik/upravitUzivatele';
							break;

							case 'odebrat-uzivatele':
								$str = 'php/technik/odebratUzivatele';
							break;
	
							case 'pridej-hw':
								$str = 'php/technik/createHWForm';
							break;
						
							case 'odebrat-hw':
								$str = 'php/technik/removeHW';
							break;

							case 'vadny-hw':
								$str = 'php/technik/vadnyHW';
							break;

							case 'upravit-hw':
								$str = 'php/technik/upravitHW';
							break;

							case 'opravit-hw':
								$str = 'php/technik/opravitHW';
							break;
	
							case 'pridat-kancelar':
								$str = 'php/technik/pridatKancelar';
							break;
	
							case 'kancelar-uzivatel':
								$str = 'php/technik/kancelarUzivatel';
							break;
	
							case 'kancelar-prehled':
								$str = 'php/technik/kancelarPrehled';
							break;
	
							case 'upravit-kancelar':
								$str = 'php/technik/upravitKancelar';
							break;

							case 'odebrat-kancelar':
								$str = 'php/technik/odebratKancelar';
							break;

							case 'pridat-ucebna':
								$str = 'php/technik/pridatUcebnu';
							break;

							case 'upravit-ucebna':
								$str = 'php/technik/upravitUcebnu';
							break;

							case 'odebrat-ucebna':
								$str = 'php/technik/odebratUcebnu';
							break;

							default:
								$str = 'php/error404';
							break;
						}
					}
				}
				else{
					$str = "php/error404";
				}
				
                include "php/technik/technikMenu.php";

            }
            else if($_SESSION["role"] == 2){	//uzivatel
                if($str == "uzivatel" || $str == "logIn" || $str == "home"){
                	$str = "php/uzivatel/nahlasitVadnyHw";
					
					if(isset($_GET['option']) && !empty($_GET['option'])){
						switch($_GET['option']){

							case 'vypis-dostupny':
								$str = 'php/uzivatel/vypisDostupnehoHw';
							break;

							case 'vypis-technici':
								$str = 'php/uzivatel/vypisTechniciUceben';
							break;

							case 'vypis-k-oprave':
								$str = 'php/uzivatel/hwNahlasenKOprave';
							break;
						
							case 'historie-hlaseni':
								$str = 'php/uzivatel/historieHlaseni';
							break;
						
							default:
								$str = 'php/error404';
							break;
						}
					}
				}
				else{
					$str = "php/error404";
				}
				
                include "php/uzivatel/uzivatelMenu.php";
            }
			else{
				$str = "php/error404";
			}
            
               echo '<div class="col-md-10 col-sm-8 col-xs-12">';
                    $file=$str.".php";
                    include $file;
               echo '</div>';
        }
    }
}
else{
	if($_SESSION["role"] == 1){
		echo "<div class='row'>";
		include "php/technik/technikMenu.php";
		echo '<div class="col-md-10 col-sm-8 col-xs-12">';		
		include "php/technik/technik.php";
		echo '</div>';
		echo "</div>";
	} // technik
	else if($_SESSION["role"] == 2){
		echo "<div class='row'>";
		include "php/uzivatel/uzivatelMenu.php";
		echo '<div class="col-md-10 col-sm-8 col-xs-12">';
		include "php/uzivatel/nahlasit_vadny_hw.php";
		echo "</div>";
		echo "</div>";
	} // uzivatel
    else
    	include "php/logIn.php";
}

	echo "</div>";

	include "php/footer.php"; 
?>
</div>
</body>
</html>
