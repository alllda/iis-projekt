SET FOREIGN_KEY_CHECKS=0;
SET NAMES utf8;

drop table if exists technik;
drop table if exists ucebnaCVT;
drop table if exists hardware;
drop table if exists uzivatel;
drop table if exists kancelar;
drop table if exists oprava;
drop table if exists vlastnik;


create table kancelar
( k_id    int primary key auto_increment,
  patro_k int not null,
  blok_k  varchar(1) not null
);

create table hardware
( hw_id   int primary key auto_increment,
  nazev   varchar(64) not null,
  typ     varchar(64) not null,
  vyrobce varchar(64) not null,
  cena    int not null
);

create table technik
( tech_id     int primary key auto_increment,
  jmeno_t     varchar(64) not null,
  prijmeni_t  varchar(64) not null,
  login_t     varchar(64) not null,
  kancelar_id int,
  heslo_t    varchar(64) not null,
  foreign key(kancelar_id) references kancelar(k_id)
);

create table uzivatel
( uziv_id     int primary key auto_increment,
  jmeno       varchar(64) not null,
  prijmeni    varchar(64) not null,
  login_u     varchar(64) not null,
  kancelar_id int,
  titul       varchar(32),
  heslo_u    varchar(64) not null,
  foreign key(kancelar_id) references kancelar(k_id)
);

create table ucebnaCVT
( ucebna_id     int primary key auto_increment,
  blok          varchar(1) not null,
  patro         int not null,
  kapacita      int not null,
  pocet_tabuli  int not null,
  spravce_id    int not null,
  foreign key(spravce_id) references technik(tech_id)
);

create table oprava
( id_opravy       int primary key auto_increment,
  datum_vlozeni   TIMESTAMP default now(),
  datum_vyrizeni  TIMESTAMP,
  poznamka        varchar(128) not null,
  id_hardware     int not null,
  id_uzivatele    int not null,
  id_technika     int ,
  foreign key(id_hardware) references hardware(hw_id),
  foreign key(id_uzivatele) references uzivatel(uziv_id),
  foreign key(id_technika) references technik(tech_id)
);

create table vlastnik
( id_vlatneni   int primary key auto_increment,
  id_HW         int not null,
  id_ucebny     int default null,
  id_uzivatele  int default null,
  foreign key(id_HW) references hardware(hw_id),
  foreign key(id_ucebny) references ucebnaCVT(ucebna_id),
  foreign key(id_uzivatele) references uzivatel(uziv_id)
);



insert into kancelar values(NULL,1,'A');
insert into kancelar values(NULL,2,'A');
insert into kancelar values(NULL,1,'F');
insert into kancelar values(NULL,3,'D');

insert into hardware values(NULL,'BenQ LCD','A84-D','BenQ','4860');
insert into hardware values(NULL,'Klavesnice','78-95G','Genius','320');
insert into hardware values(NULL,'Notebook','E530','Lenovo','13654');
insert into hardware values(NULL,'Myš','BNE-3250','Logitech','580');

insert into technik values(NULL,'Lojza','Marny','log',1,'pass');
insert into technik values(NULL,'Mirek','Novák','log2',2,'pass');
insert into technik values(NULL,'Karel','Smetana','log3',1,'pass');
insert into technik values(NULL,'Vašek','Dvořák','log4',3,'pass');

insert into ucebnaCVT values(NULL,'K',1,32,1,1);
insert into ucebnaCVT values(NULL,'J',2,22,1,2);
insert into ucebnaCVT values(NULL,'K',2,10,0,1);
insert into ucebnaCVT values(NULL,'J',1,32,2,3);

insert into uzivatel values(NULL,'Miloš','Beneš','login',1,'Ing.','passwd');
insert into uzivatel values(NULL,'Martin','Kučera','login2',null,null,'passwd');
insert into uzivatel values(NULL,'Lukas','Labaj','login3',2,'Mgr.','passwd');
insert into uzivatel values(NULL,'Lenka','Kratochvílová','login4',1,'Ing.','passwd');

insert into vlastnik values(NULL,1,1,null);
insert into vlastnik values(NULL,2,1,null);
insert into vlastnik values(NULL,3,null,2);
insert into vlastnik values(NULL,4,2,null);

insert into oprava values(NULL,'2014-4-21 16:00:00',0,'Nejede',1,1,null);
insert into oprava values(NULL,'2013-10-5 15:00:00','2014-11-5 18:00:00','Vydava zvuky',1,1,2);
insert into oprava values(NULL,'1995-8-13 14:00:00',0,'Blika',2,1,null);
insert into oprava values(NULL,'2000-5-6 13:00:00',0,'Je to zkazene',3,1,null);
